# alpine-netatalk

#### [alpine-x64-netatalk](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-netatalk/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinex64/alpine-x64-netatalk.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-netatalk "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinex64/alpine-x64-netatalk.svg)](https://microbadger.com/images/forumi0721alpinex64/alpine-x64-netatalk "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-netatalk.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-netatalk/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-netatalk.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-netatalk/)
#### [alpine-aarch64-netatalk](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-netatalk/)
[![](https://images.microbadger.com/badges/version/forumi0721alpineaarch64/alpine-aarch64-netatalk.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-netatalk "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpineaarch64/alpine-aarch64-netatalk.svg)](https://microbadger.com/images/forumi0721alpineaarch64/alpine-aarch64-netatalk "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-netatalk.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-netatalk/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-netatalk.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-netatalk/)
#### [alpine-armhf-netatalk](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-netatalk/)
[![](https://images.microbadger.com/badges/version/forumi0721alpinearmhf/alpine-armhf-netatalk.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-netatalk "Get your own version badge on microbadger.com") [![](https://images.microbadger.com/badges/image/forumi0721alpinearmhf/alpine-armhf-netatalk.svg)](https://microbadger.com/images/forumi0721alpinearmhf/alpine-armhf-netatalk "Get your own image badge on microbadger.com") [![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-netatalk.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-netatalk/) [![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-netatalk.svg?style=flat-square)](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-netatalk/)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [Netatalk](http://netatalk.sourceforge.net/)
    - Netatalk is a freely-available Open Source AFP fileserver.
* Base Image
    - [forumi0721/alpine-x64-base](https://hub.docker.com/r/forumi0721/alpine-x64-base/)
    - [forumi0721/alpine-aarch64-base](https://hub.docker.com/r/forumi0721/alpine-aarch64-base/)
    - [forumi0721/alpine-armhf-base](https://hub.docker.com/r/forumi0721/alpine-armhf-base/)



----------------------------------------
#### Run

* x64
```sh
docker run -d \
           -p 548:548/tcp \
           -v /data:/data \
           -e USER_NAME=username \
           -e USER_PASSWD=passwd \
           -e AFP_HOSTNAME="afp server" \ 
           -e AFP_SHARE="data=/data=[size],..." \ 
           -e AFP_TIMEMACHINE="timemachine=/timemachine=[size],..." \ 
           forumi0721alpinex64/alpine-x64-netatalk:latest
```

* aarch64
```sh
docker run -d \
           -p 548:548/tcp \
           -v /data:/data \
           -e USER_NAME=username \
           -e USER_PASSWD=passwd \
           -e AFP_HOSTNAME="afp server" \ 
           -e AFP_SHARE="data=/data=[size],..." \ 
           -e AFP_TIMEMACHINE="timemachine=/timemachine=[size],..." \ 
           forumi0721alpineaarch64/alpine-aarch64-netatalk:latest
```

* armhf
```sh
docker run -d \
           -p 548:548/tcp \
           -v /data:/data \
           -e USER_NAME=username \
           -e USER_PASSWD=passwd \
           -e AFP_HOSTNAME="afp server" \ 
           -e AFP_SHARE="data=/data=[size],..." \ 
           -e AFP_TIMEMACHINE="timemachine=/timemachine=[size],..." \ 
           forumi0721alpinearmhf/alpine-armhf-netatalk:latest
```



----------------------------------------
#### Usage

* mount share directory
    - Linux : `mount_afp -o user=username,volpass=passwd afp://username:passwd@localhost/data /mnt/data` 
    - Darwin : `mount afp://username:passwd@$localhost/data /Volumes/data`
    - Default username/password : forumi0721/passwd

* If you want to use multiple user or complex setting, you need to create `afp.conf` and add `-v afp.conf:/etc/afp.conf` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 548/tcp            | Listen port for afpd daemon                      |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /data              | Netatalk share directory                         |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| USER_NAME          | Login username (default : forumi0721)            |
| USER_PASSWD        | Login password (default : passwd)                |
| USER_EPASSWD       | Login password (base64)                          |
| AFP_HOSTNAME       | Netatalk hostname (default : afp server)         |
| AFP_SHARE          | Netatalk share directory (default : /data)       |
| AFP_TIMEMACHINE    | Netatalk timemachine support volume              |



----------------------------------------
* [forumi0721alpinex64/alpine-x64-netatalk](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-netatalk/)
* [forumi0721alpinearmhf/alpine-armhf-netatalk](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-netatalk/)
* [forumi0721alpineaarch64/alpine-aarch64-netatalk](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-netatalk/)

